library ieee;
use ieee.std_logic_1164.all;

entity PushButtonLED is
	Port (BUTTON1 : in std_logic; // BUTTON1 input node
			BUTTON2 : in std_logic; // BUTTON2 input node
			LED1 : out std_logic; // LED1 output node
			LED2 : out std_logic); // LED2 output node
end PushButtonLED;

-- LED 1 = pin 3
-- LED 2 = pin 7

architecture Behavioral of PushButtonLED is
begin
	LED1 <= not BUTTON1;
	LED2 <= not BUTTON2;
end Behavioral;