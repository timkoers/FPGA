-----------------------------------------------------------------------------
-- Title      : I2C_slave IIC_Slave
-----------------------------------------------------------------------------
-- File       : IIC_ESP
-- Author     : Peter Samarin <peter.samarin@gmail.com>
-----------------------------------------------------------------------------
-- Copyright (c) 2016 Peter Samarin
-----------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use work.txt_util.all;
use ieee.math_real.all;  -- using uniform(seed1,seed2,rand)
------------------------------------------------------------------------
entity IIC_ESP is
	port (
		clk 							: in std_logic;
		sda   						: inout std_logic :='0';     ---i2c data line
		scl   						: inout std_logic :='0';       ---i2c clock line
		rst 							: in std_logic;
		FPGA_INPUT_0				: in std_logic :='0';
		FPGA_INPUT_1				: in std_logic :='0';
		LED_OUTPUT					: out std_logic := '0'
);
end IIC_ESP;

------------------------------------------------------------------------
architecture Behavioral of IIC_ESP is
 
	signal state_dbg            : integer                      := 0;
   signal received_data        : std_logic_vector(7 downto 0) := (others => '0');
   signal ack                  : std_logic                    := '0';
   signal read_req             : std_logic                    := '0';
   signal data_to_master       : std_logic_vector(7 downto 0) := (others => '0');
   signal data_valid           : std_logic                    := '0';
   signal data_from_master     : std_logic_vector(7 downto 0) := (others => '0');
   signal LED_OUT     			 : std_logic 						  := '0';
  
	function to_integer( s : std_logic ) return natural is
	begin
		if s = '1' then
			return 1;
		else
			return 0;
		end if;
	end function;
  
	begin

	LED_OUTPUT <= LED_OUT;

	
  ---- Design Under Verification -----------------------------------------
  I2C_slave : entity work.I2C_slave
    generic map (
      SLAVE_ADDR => "0101111")
    port map (
      -- I2C
      scl              => scl,
      sda              => sda,
      -- default signals
      clk              => clk,
      rst              => clk,
      -- user interface
      read_req         => read_req,
      data_to_master   => data_to_master,
      data_valid       => data_valid,
      data_from_master => data_from_master);

----------------------------------------------------------
-- Save data received from the master in a register
----------------------------------------------------------
process (clk, rst, read_req, data_to_master, data_valid, data_from_master, FPGA_INPUT_0, FPGA_INPUT_1, LED_OUT) is
  begin
  
	if (FPGA_INPUT_0 = '1') then
			LED_OUT <= '0';
	end if;
	if (FPGA_INPUT_1 = '1') then
		LED_OUT <= '1';
	end if;	
  
	if data_valid = '1' then

		case data_from_master is
				
			when "00000001" =>
				data_to_master <= std_logic_vector(to_unsigned(to_integer(FPGA_INPUT_0),8));
				read_req <= '1';
				
			when "00000010" =>
					data_to_master <= std_logic_vector(to_unsigned(to_integer(FPGA_INPUT_1),8));
					read_req <= '1';
				
			when "00000011"=>
				data_to_master <= std_logic_vector(to_unsigned(to_integer(LED_OUT),8));
				read_req <= '1';
				
			when others =>
		end case;
	end if;
end process;

end Behavioral;

