#include <Wire.h>

#define ADDRESS 0x2F

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Wire.begin(21, 22);
  Serial.println("Setup done!");
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("Asking for button 1");

  Wire.beginTransmission(ADDRESS); // 0x2F
  Wire.write(0x1); // Button 1 state
  Wire.endTransmission();
  Wire.requestFrom(ADDRESS, 8);

  while (!Wire.available());

  Serial.printf("Button 1 state %d\n", Wire.read());

  Serial.println("Asking for button 2");

  Wire.beginTransmission(ADDRESS); // 0x2F
  Wire.write(0x2); // Button 2 state
  Wire.endTransmission();
  Wire.requestFrom(ADDRESS, 8);

  while (!Wire.available());

  Serial.printf("Button 2 state %d\n", Wire.read());

  Serial.println("Asking for led");

  Wire.beginTransmission(ADDRESS); // 0x2F
  Wire.write(0x3); // Led state
  Wire.endTransmission();
  Wire.requestFrom(ADDRESS, 8);
  
  while (!Wire.available());

  Serial.printf("LED output state %d\n", Wire.read());

  delay(300);

}
