library IEEE;
 use IEEE.STD_LOGIC_1164.ALL;
 use IEEE.STD_LOGIC_ARITH.ALL;
 use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity IIC_RTC is
 port ( clk   : in std_logic;               -- The 50Mhz Clk and the "clk_count_400hz" counter variable are used to generate a 400Hz clock pulse 
														-- to drive the LCD CORE state machine.
    reset : in std_logic;               ---switch i/p
	 sda   : inout std_logic := '0';     ---i2c data line
	 scl   : out std_logic := '0';       ---i2c clock line
	 do    : out std_logic := '1';       ---uart transmit line
	 l1    : out std_logic := '0';
		
	 lcd_rs             : OUT    std_logic;
	 lcd_e              : OUT    std_logic;
	 lcd_rw             : OUT    std_logic;
	 lcd_on             : OUT    std_logic;
	 lcd_blon           : OUT    std_logic; -- Backlight on

	 data_bus_0         : INOUT  STD_LOGIC;
	 data_bus_1         : INOUT  STD_LOGIC;
	 data_bus_2         : INOUT  STD_LOGIC;
	 data_bus_3         : INOUT  STD_LOGIC;
	 data_bus_4         : INOUT  STD_LOGIC;
	 data_bus_5         : INOUT  STD_LOGIC;
	 data_bus_6         : INOUT  STD_LOGIC;
	 data_bus_7         : INOUT  STD_LOGIC
  
  );      ---led display
end IIC_RTC;

architecture Behavioral of IIC_RTC is 
 type states is (start,res,dat,stop,judge,secondstata1,day,data1,final,waity);  --fsm
 signal stata : states := start;
 constant control1 : std_logic_vector(7 downto 0) := x"d0";  --command for write
 signal add : std_logic_vector(7 downto 0) := "00000000";                       --x"01" ;
 constant control2 : std_logic_vector(7 downto 0) := x"d1"; ---command for read
 signal stores : std_logic_vector(7 downto 0) := x"00";      --storing of second
 signal storem : std_logic_vector(7 downto 0) := x"00";      --storing of minute
 signal storeh : std_logic_vector(7 downto 0) := x"00";      --storing of hour
 signal storeda : std_logic_vector(7 downto 0):= x"01";      --storing of day
 signal storedate : std_logic_vector(7 downto 0):= x"00";    --storing of date
 signal storemon : std_logic_vector(7 downto 0):= x"00";     --storing of month
 signal storeye : std_logic_vector(7 downto 0):= x"00";      --storing of year
 type arrss is array(0 to 31) of std_logic_vector(7 downto 0);
 signal store1 : arrss :=(x"54",x"49",x"4D",x"45",x"20",x"3a",x"20",x"00",x"00",x"3a",
 x"00",x"00",x"3a",x"00",x"00",x"20",x"00",x"00",x"00",x"20",x"00",x"00",x"2d",x"00",
 x"00",x"2d",x"32",x"30",x"00",x"00",x"20",x"20");  


 type state_type is (func_set, display_on, mode_set, print_string,
  line2, return_home, drop_lcd_e, reset1, reset2,
   reset3, display_off, display_clear );
   
 type menu_state is (settings_led_on, settings, stop_process, start_process);
  
 signal state, next_command         : state_type;
    
  
 signal data_bus_value, next_char   : STD_LOGIC_VECTOR(7 downto 0);
 signal clk_count_400hz             : STD_LOGIC_VECTOR(23 downto 0);
 signal char_count                  : STD_LOGIC_VECTOR(4 downto 0);
 signal clk_400hz_enable,lcd_rw_int : std_logic;
   
 signal data_bus                    : STD_LOGIC_VECTOR(7 downto 0);

 signal ack : std_logic;  
 
begin

 --===================================================--  
-- SIGNAL STD_LOGIC_VECTORS assigned to OUTPUT PORTS 
--===================================================--    

data_bus_0 <= data_bus(0);
data_bus_1 <= data_bus(1);
data_bus_2 <= data_bus(2);
data_bus_3 <= data_bus(3);
data_bus_4 <= data_bus(4);
data_bus_5 <= data_bus(5);
data_bus_6 <= data_bus(6);
data_bus_7 <= data_bus(7);

-- BIDIRECTIONAL TRI STATE LCD DATA BUS
data_bus <= data_bus_value when lcd_rw_int = '0' else "ZZZZZZZZ";
   
-- LCD_RW PORT is assigned to it matching SIGNAL 
lcd_rw <= lcd_rw_int;

 process(reset,clk,sda)
  variable a,i,y,z : integer := 0;
  variable n : integer := 7;
  begin
   if reset = '1' then
    if clk'event and clk = '1' then
     if stata = start then    ----start condition
      if i <= 250 then
       i := i + 1;
       sda <= '1';
       scl <= '1';
      elsif i > 250 and i < 500 then
       i := i + 1;
       sda <= '0';
       scl <= '1';
      elsif i = 500 then
       i := 0;
       sda <= '0';
       scl <= '0';
       stata <= res ;
      end if;
     end if;
     
     if stata = res then
      if i <= 250 then  ----command and address send state
       i := i + 1;
       scl <= '0';
       if n > -1 then
        if a = 0 then
         sda <= control1(n);   ----write command 
        elsif a = 1 then
         sda <= add(n);        ----address
        elsif a = 2 then
         sda <= control2(n);   ----read command
        end if;
       elsif n = -1 then
        sda <= 'Z';
        ack <= sda;
       end if;
      elsif i > 250 and i <= 500 then
       i := i + 1;
       scl <= '1';
       if n > -1 then
        if a = 0 then
         sda <= control1(n);
        elsif a = 1 then
         sda <= add(n);
        elsif a = 2 then
         sda <= control2(n);
        end if;
       elsif n = -1 then
        sda <= 'Z';
        ack <= sda;
       end if;
      elsif i > 500 and i < 750 then
       i := i + 1;
       scl <= '0';
       if n > -1 then
        if a = 0 then
         sda <= control1(n);
        elsif a = 1 then
         sda <= add(n);
        elsif a = 2 then
         sda <= control2(n);
        end if;
       elsif n = -1 then
        sda <= 'Z';
        ack <= sda;
       end if;
      elsif i = 750 then
       scl <= '0';
       i := 0;
       if n > -1 then
        n := n - 1;
       elsif n = -1 then
        n := 7;
        if a = 0 then
         a := 1;
         stata <= res;
        elsif a = 1 then
         a := 2; 
         stata <= start;
        elsif a = 2 then
         a := 0 ;
         stata <= dat;
        end if;
       ----------------------------------------------------------------
       end if;
      end if;
     end if;

     if stata = dat then  ----initial data for all seven register send module
      if i <= 250 then
       i := i + 1;
       scl <= '0';
       if n > -1 then
        sda <= 'Z';
       elsif n = -1 then
        sda <= '1';
       end if;
      elsif i > 250 and i <= 500 then
       i := i + 1;
       scl <= '1';
       if n > -1 then
        sda <= 'Z';
        case add is 
         when x"00" =>
         stores(n) <= sda;
         ------------------------------------------------
         when x"01" =>
          storem(n) <= sda;
         when x"02" =>
          storeh(n) <= sda;
         when x"03" =>
          storeda(n) <= sda;
         when x"04" =>
          storedate(n) <= sda;
         when x"05" =>
          storemon(n) <= sda;
          l1 <= '1';
         when x"06" =>
          storeye(n) <= sda;
         when others =>
          null;
        end case;
        ----------------------------------------------------
       elsif n = -1 then
        sda <= '1';
       end if;
      elsif i > 500 and i < 750 then
       i := i + 1;
       scl <= '0';
       if n > -1 then
        sda <= 'Z';
       elsif n = -1 then
        sda <= '1';
       end if;
      elsif i = 750 then
       scl <= '0';
       i := 0;
       if n > -1 then
        n := n - 1;
       elsif n = -1 then
        n := 7;
        stata <= stop;
       end if;
      end if;
     end if;
     -------------------------------------------------------------------------
     if stata = stop then   ------stop condition state
      ------------------------------------------------------------
      if i <= 250 then
       i := i + 1;
       scl <= '1';
       sda <= '0';
      elsif i > 250 and i <= 500 then
       i := i + 1;
       scl <= '1';
       sda <= '1';
      elsif i > 500 and i < 750 then
       i := i + 1;
       scl <= '0';
       sda <= '0';
      elsif i = 750 then
       scl <= '0';
       sda <= '0';
       i := 0;
       stata <= judge;
      end if;
     end if;
     ------------------------------------------------
     if stata = judge then   ----various flow path determination
      case  add is 
       when  x"00" =>                      
        stata <=  start;
        add <= x"01" ;
       when  x"01" =>                           
        stata <= start;
        add <= x"02" ;
       when  x"02" =>                            
        stata <= start;
        add <= x"03" ;
       when  x"03" =>                              
        stata <= start;
        add <= x"04" ;
       when  x"04" =>
        stata <= start;
        add <= x"05";
       when  x"05" =>
        stata  <= start;
        add <= x"06";
       when  x"06" =>
        stata  <= secondstata1;
        add <= x"00";
       when others =>
       null;
      end case;
     end if;
     ----------------------------------------------------------------------transmit
     if stata = secondstata1 then    ---conversion of received data from i2c to ascii code
      store1(13) <= "00110" & stores(6 downto 4);
      store1(14) <= "0011" & stores(3 downto 0);
      store1(10) <= "00110" & storem(6 downto 4);
      store1(11) <= "0011" & storem(3 downto 0);
      store1(7) <= "001100" & storeh(5 downto 4);
      store1(8) <= "0011" & storeh(3 downto 0);
      stata <= day ;
     end if;
     if stata = day then     ---converting the received data from i2c to days
      if storeda(2 downto 0) = "001" then
      store1(16) <= "01010011"; -- S
      store1(17) <= "01010101"; -- U
      store1(18) <= "01001110"; -- N
      elsif storeda(2 downto 0) = "010" then
      store1(16) <= "01001101"; -- M
      store1(17) <= "01001111"; -- O
      store1(18) <= "01001110"; -- N
      elsif storeda(2 downto 0) = "011" then
      store1(16) <= "01010100"; -- T
      store1(17) <= "01010101"; -- U
      store1(18) <= "01000101"; -- E
      elsif storeda(2 downto 0) = "100" then
      store1(16) <= "01010111"; -- W
      store1(17) <= "01000101"; -- E
      store1(18) <= "01000100"; -- D
      elsif storeda(2 downto 0) = "101" then
      store1(16) <= "01010100"; -- T
      store1(17) <= "01001000"; -- H
      store1(18) <= "01010101"; -- U
      elsif storeda(2 downto 0) = "110" then
      store1(16) <= "01000110"; -- F
      store1(17) <= "01010010"; -- R
      store1(18) <= "01001001"; -- I
      elsif storeda(2 downto 0) = "111" then
      store1(16) <= "01010010"; -- S
      store1(17) <= "01000001"; -- A
      store1(18) <= "01010100"; -- T
      end if;
      stata <= data1;
     end if;
     -----------------------------------------------------------------------------------
     if stata = data1 then
      store1(20) <= "001100" & storedate(5 downto 4);
      store1(21) <= "0011"    & storedate(3 downto 0);
      store1(23) <= "0011000" & storemon(4);
      store1(24) <= "0011"    & storemon(3 downto 0);
      store1(28) <= "0011"    &  storeye(7 downto 4);
      store1(29) <= "0011"    & storeye(3 downto 0);
      stata <= final;
     end if;
     -----------------------------------------------------------------------------------
     if stata = final then
      scl <= '0';
      sda <= '0';
      if i < 5200 then   ----timing delay for each bit in uart controller
       i := i + 1;
       if z = 0 then
         do <= '0';
       elsif z = 1 then
        do <= store1(y)(0);
       elsif z = 2 then
        do <= store1(y)(1);
       elsif z = 3 then
        do <= store1(y)(2);
       elsif z = 4 then
        do <= store1(y)(3);
       elsif z = 5 then
        do <= store1(y)(4);
       elsif z = 6 then
        do <= store1(y)(5);
       elsif z = 7 then
        do <= store1(y)(6);
       elsif z = 8 then
        do <= store1(y)(7);
       elsif z = 9 then
        do <= '1';
       end if;
      elsif i = 5200 then
       i := 0;
       if z <= 8 then
        z := z + 1;
       elsif z > 8 then
        z := 0 ;
        if y < 30 then       ---14
        y := y + 1;
        elsif y = 30 then
        y := 0;
        stata  <=  waity;                                 --waity;
        end if;
       end if;
      end if;
     end if;
     ------------------------------------------------------------------
     if stata = waity then     ---delay before reading repeatedly again the seven registers
      scl <= '0';
      sda <= '0';
      if i < 50000000 then
       i := i + 1;
      elsif i = 50000000 then
       i := 0 ;
       stata  <= start;
      end if;
     end if;
     -----------------------------------------------
   end if;
  end if;
  ----------------------------------------------------------
end process;


--======================= CLOCK SIGNALS ============================--  
process(clk)
begin
      if (rising_edge(clk)) then
         if (reset = '0') then
            clk_count_400hz <= x"000000";
            clk_400hz_enable <= '0';
         else

--== NOTE for the IF statement below:
----------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
-- Due to the fact that each STATE in the LCD Driver State Machine... (Shown further below) ...each state will immediately be followed by the "drop_lcd_e" STATE....
-- this will cause the Period of the "lcd_e" signal to be doubled when viewed on an Oscilloscope. This also causes your set frequency value 
-- to be divided in half. The variable "clk_count_400hz" whichever hex value you choose for a specific Frequency, that frequency will be divided in half.  
-- i.e: (clk_count_400hz <= x"7A120") is the value for a 100hz signal, however when you monitor the LCD's "ENABLE" port with an oscilloscope; it will be detected as 
-- a 50Hz signal. (Half the Set Frequency). This is becasue the State machine cycles twice!!! Creating 1 Full Cycle for the "LCD_E" Port. Logic HI...then....LOGIC LOW.
--======================================================================================================================================================================             
           
          if (clk_count_400hz <= x"00F424") then            
                                                 -- If using the DE2 50Mhz Clock,  use clk_count_1600hz <= x"007A12"  (50Mhz/1600hz = 32250   converted to HEX = 7A12 )
                                                 --                                use clk_count_1250hz <= x"009C40"  (50Mhz/1250hz = 40000   converted to HEX = 9C40 )
                                                 --                                use clk_count_800hz  <= x"00F424"  (50Mhz/800hz  = 62500   converted to HEX = F424 )
                                                 --                                use clk_count_400hz  <= x"01E848"  (50Mhz/400hz  = 125000  converted to HEX = 1E848 )
                                                 --                                use clk_count_200hz  <= x"03D090"  (50Mhz/200hz  = 250000  converted to HEX = 3D090 )
                                                 --                                use clk_count_100hz  <= x"07A120"  (50Mhz/100hz  = 500000  converted to HEX = 7A120 )
                                                 --                                use clk_count_50hz   <= x"0F4240"  (50Mhz/50hz   = 1000000 converted to HEX = F4240 )
                                                 --                                use clk_count_10hz   <= x"4C4B40"  (50Mhz/10hz   = 5000000 converted to HEX = 4C4B40 )
                                                           
                                                 --  In Theory for a 27Mhz Clock,  use clk_count_400hz <= x"107AC"  (27Mhz/400hz = 67500  converted to HEX = 107AC )
                                                 --                                use clk_count_200hz <= x"20F58"  (27Mhz/200hz = 125000 converted to HEX = 20F58 )
                                                             
                                                 --  In Theory for a 25Mhz Clock.  use clk_count_400hz <= x"00F424"  (25Mhz/400hz = 62500  converted to HEX = F424 )
                                                 --                                use clk_count_200hz <= x"01E848"  (25Mhz/200hz = 125000 converted to HEX = 1E848 )
                                                 --                                use clk_count_100hz <= x"03D090"  (25Mhz/100hz = 250000 converted to HEX = 3D090 )
                                                           
                                                           
                   clk_count_400hz <= clk_count_400hz + 1;                                          
                   clk_400hz_enable <= '0';                
           else
                   clk_count_400hz <= x"000000";
                   clk_400hz_enable <= '1';
            end if;
         end if;
			
			
      end if;
		
end process;  
--==================================================================--    
  
--======================== LCD DRIVER CORE ==============================--   
--                     STATE MACHINE WITH RESET                          -- 
--===================================================-----===============--  
process (clk, reset)
begin
        if reset = '0' then
           state <= reset1;
           data_bus_value <= x"38"; -- RESET
           next_command <= reset2;
           lcd_e <= '1';
           lcd_rs <= '0';
           lcd_rw_int <= '0';  
    
    
    
        elsif rising_edge(clk) then
             if clk_400hz_enable = '1' then  
             
					next_char <= store1(CONV_INTEGER(char_count));
	
              --========================================================--                 
              -- State Machine to send commands and data to LCD DISPLAY
              --========================================================--
                 case state is
                 -- Set Function to 8-bit transfer and 2 line display with 5x8 Font size
                 -- see Hitachi HD44780 family data sheet for LCD command and timing details
                       
                       
                       
--======================= INITIALIZATION START ============================--
                       when reset1 =>
                            lcd_e <= '1';
                            lcd_rs <= '0';
                            lcd_rw_int <= '0';
                            data_bus_value <= x"38"; -- EXTERNAL RESET
                            state <= drop_lcd_e;
                            next_command <= reset2;
                            char_count <= "00000";
  
                       when reset2 =>
                            lcd_e <= '1';
                            lcd_rs <= '0';
                            lcd_rw_int <= '0';
                            data_bus_value <= x"38"; -- EXTERNAL RESET
                            state <= drop_lcd_e;
                            next_command <= reset3;
                            
                       when reset3 =>
                            lcd_e <= '1';
                            lcd_rs <= '0';
                            lcd_rw_int <= '0';
                            data_bus_value <= x"38"; -- EXTERNAL RESET
                            state <= drop_lcd_e;
                            next_command <= func_set;
            
            
                       -- Function Set
                       --==============--
                       when func_set =>                
                            lcd_e <= '1';
                            lcd_rs <= '0';
                            lcd_rw_int <= '0';
                            data_bus_value <= x"38";  -- Set Function to 8-bit transfer, 2 line display and a 5x8 Font size
                            state <= drop_lcd_e;
                            next_command <= display_off;
                            
                            
                            
                       -- Turn off Display
                       --==============-- 
                       when display_off =>
                            lcd_e <= '1';
                            lcd_rs <= '0';
                            lcd_rw_int <= '0';
                            data_bus_value <= x"08"; -- Turns OFF the Display, Cursor OFF and Blinking Cursor Position OFF.......
                                                     -- (0F = Display ON and Cursor ON, Blinking cursor position ON)
                            state <= drop_lcd_e;
                            next_command <= display_clear;
                           
                           
                       -- Clear Display 
                       --==============--
                       when display_clear =>
                            lcd_e <= '1';
                            lcd_rs <= '0';
                            lcd_rw_int <= '0';
                            data_bus_value <= x"01"; -- Clears the Display    
                            state <= drop_lcd_e;
                            next_command <= display_on;
                           
                           
                           
                       -- Turn on Display and Turn off cursor
                       --===================================--
                       when display_on =>
                            lcd_e <= '1';
                            lcd_rs <= '0';
                            lcd_rw_int <= '0';
                            data_bus_value <= x"0C"; -- Turns on the Display (0E = Display ON, Cursor ON and Blinking cursor OFF) 
                            state <= drop_lcd_e;
                            next_command <= mode_set;
                          
                          
                       -- Set write mode to auto increment address and move cursor to the right
                       --====================================================================--
                       when mode_set =>
                            lcd_e <= '1';
                            lcd_rs <= '0';
                            lcd_rw_int <= '0';
                            data_bus_value <= x"06"; -- Auto increment address and move cursor to the right
                            state <= drop_lcd_e;
                            next_command <= print_string; 
                            
                                
								--======================= INITIALIZATION END ============================-- 

								                              
--======================= INITIALIZATION END ============================--                          
                          
                          
                          
                          
--=======================================================================--                           
--               Write ASCII hex character Data to the LCD
--=======================================================================--
                       when print_string =>          
                            state <= drop_lcd_e;
                            lcd_e <= '1';
                            lcd_rs <= '1';
                            lcd_rw_int <= '0';
                          
                          
                               -- ASCII character to output
                               -----------------------------
                               -- Below we check to see if the Upper-Byte of the HEX number being displayed is x"0"....We use this number x"0" as a Control Variable, 
                               -- to know when a certain condition is met.  Next, we proceed to process the "next_char" variable to Sequentially count up in HEX format.
                               
                               -- This is required because as you know...Counting in HEX...after #9 comes Letter A.... well if you look at the ASCII CHART, 
                               -- the Letters A,B,C etc. are in a different COLUMN compared to the one the Decimal Numbers are in.  Letters A...F are in Column  x"4".    
                               -- Numbers 0...9 are in Column x"3".  The Upper-Byte controls which COLUMN the Character will be selected from... and then Displayed on the LCD. 
                               
                               -- So to Count up seamlessly using our 4-Bit Variable 8,9,10,11 and so on...we need to set some IF THEN ELSE conditions 
                               -- to control this changing of Columns so that it will be displayed counting up in HEX Format....8,9,A,B,C,D etc.
                                
                               -- Also, if the High-Byte is detected as an actual Character Column from the ASCII CHART that has Valid Characters 
                               -- (Like using a Upper-Byte of x"2",x"3",x"4",x"5",x"6" or x"7") then it will just go ahead and decalre  "data_bus_value <= next_char;"  
                               -- and the "Print_Sring" sequence will continue to execute. These HEX Counting conditions are only being applied to the Variables that have 
                               -- the x"0" Upper-Byte value.....For our code that is the:  [x"0"&hex_display_data]  variable.  
                               
                               
                               if (next_char(7 downto 4) /= x"0") then
                                  data_bus_value <= next_char;
                               else
                             
                                    -- Below we process a 4-bit STD_LOGIC_VECTOR that is counting up Sequentially, we process the values so that it Displays in HEX Format as it counts up.
                                    -- In our case, our SWITCHES have been Mapped to a 4-bit STD_LOGIC_VECTOR and we have placed an Upper-Byte value of x"0" before it.
                                    -- This triggers the Process below, which will condition which numbers and letters are displayed on the LCD as the 4-Bit Variable counts up past #9 or 1001
                                    -------------------------------------------------------------------------------------------------------------------------------------------------------------
                                    
                                    -- if the number is Greater than 9..... meaning the number is now in the Realm of HEX... A,B,C,D,E,F... then
                                    if next_char(3 downto 0) >9 then 
                              
                                    -- See the ASCII CHART... Letters A...F are in Column  x"4"
                                      data_bus_value <= x"4" & (next_char(3 downto 0)-9);  
                                    else 
                                
                                    -- See the ASCII CHART... Numbers 0...9 are in Column x"3"
                                      data_bus_value <= x"3" & next_char(3 downto 0);
                                    end if;
                               end if;
                          

                            -- Loop to send out 32 characters to LCD Display (16 by 2 lines)
                               if (char_count < 31) AND (next_char /= x"fe") then
                                   char_count <= char_count +1;                            
                               else
                                   char_count <= "00000";
                               end if;
                                    
                  
                            -- Jump to second line?
                               if char_count = 15 then 
                                  next_command <= line2;
                                      
                   
                            -- Return to first line?
                               elsif (char_count = 31) or (next_char = x"fe") then
                                     next_command <= return_home;
                               else 
                                     next_command <= print_string; 
                               end if;                  
                 
                 
                       -- Set write address to line 2 character 1
                       --======================================--
                       when line2 =>
                            lcd_e <= '1';
                            lcd_rs <= '0';
                            lcd_rw_int <= '0';
                            data_bus_value <= x"c0";
                            state <= drop_lcd_e;
                            next_command <= print_string;      
                     
                     
                       -- Return write address to first character position on line 1
                       --=========================================================--
                       when return_home =>
                            lcd_e <= '1';
                            lcd_rs <= '0';
                            lcd_rw_int <= '0';
                            data_bus_value <= x"80";
                            state <= drop_lcd_e;
                            next_command <= print_string; 
                    
                    
                    
                       -- The next states occur at the end of each command or data transfer to the LCD
                       -- Drop LCD E line - falling edge loads inst/data to LCD controller
                       --============================================================================--
                       when drop_lcd_e =>
                            state <= next_command;
                            lcd_e <= '0';
                            lcd_blon <= '1';
                            lcd_on   <= '1';
                        end case;

             end if;-- CLOSING STATEMENT FOR "IF clk_400hz_enable = '1' THEN"
             
      end if;-- CLOSING STATEMENT FOR "IF reset = '0' THEN" 
		
	end process;
end Behavioral;