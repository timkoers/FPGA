module I2C( inout sda,
				output scl,
				output[27:0]out_data,
				input clk,output[2:0] ack_out, 
				output [1:0] ack_count1,
				input reset); 
				
parameter clock_div=500;// I2C clock 50MHz/(500)=100KHz 
	
//---------------------------I2C State MachineVariables---------------------- 
parameter Rxidle=0; 
parameter Rxstart =1; 
parameter RxPointeraddr=2;
parameter Rxstart1 =3; 
parameter Rxdata =4; 
parameter Rxstop =5; 
//----------------------------------------------------------------------------- 
reg chk=0; 
reg [2:0] ack; 
reg [1:0] ack_count=0; 
reg [27:0] LED; 
assign out_data=LED; 
wire [15:0] time_out;
 
always @ (time_out[3:0]) 
	begin 
		case(time_out[3:0]) 
			4'h0: LED[6:0] <= 7'b1110111; 
			4'h1: LED[6:0] <= 7'b0010010; 
			4'h2: LED[6:0] <= 7'b1011101; 
			4'h3: LED[6:0] <= 7'b1011011; 
			4'h4: LED[6:0] <= 7'b0111010; 
			4'h5: LED[6:0] <= 7'b1101011; 
			4'h6: LED[6:0] <= 7'b1101111; 
			4'h7: LED[6:0] <= 7'b1010010; 
			4'h8: LED[6:0] <= 7'b1111111; 
			4'h9: LED[6:0] <= 7'b1111010; 
			4'hA: LED[6:0] <= 7'b1111110; 
			4'hB: LED[6:0] <= 7'b1111111; 
			4'hC: LED[6:0] <= 7'b1100101; 
			4'hD: LED[6:0] <= 7'b1110111; 
			4'hE: LED[6:0] <= 7'b1101101; 
			4'hF: LED[6:0] <= 7'b1101100; 
			default:LED[6:0] <= 7'bxxxxxxx; 
		endcase 
	end 
always @ (time_out[7:4]) 
	begin 
		case(time_out[7:4]) 
			4'h0: LED[13:6] <= 7'b1110111; 
			4'h1: LED[13:6] <= 7'b0010010; 
			4'h2: LED[13:6] <= 7'b1011101; 
			4'h3: LED[13:6] <= 7'b1011011; 
			4'h4: LED[13:6] <= 7'b0111010; 
			4'h5: LED[13:6] <= 7'b1101011;
			4'h6: LED[13:6] <= 7'b1101111; 
			4'h7: LED[13:6] <= 7'b1010010;
			4'h8: LED[13:6] <= 7'b1111111; 
			4'h9: LED[13:6] <= 7'b1111010; 
			4'hA: LED[13:6] <= 7'b1111110; 
			4'hB: LED[13:6] <= 7'b1111111; 
			4'hC: LED[13:6] <= 7'b1100101; 
			4'hD: LED[13:6] <= 7'b1110111; 
			4'hE: LED[13:6] <= 7'b1101101; 
			4'hF: LED[13:6] <= 7'b1101100; 
			default:LED[13:6] <= 7'bxxxxxxx; 
		endcase 
	end 
always @ (time_out[11:8]) 
	begin 
		case(time_out[11:8]) 
			4'h0: LED[20:7] <= 7'b1110111; 
			4'h1: LED[20:7] <= 7'b0010010;
			4'h2: LED[20:7] <= 7'b1011101;
			4'h3: LED[20:7] <= 7'b1011011;
			4'h4: LED[20:7] <= 7'b0111010; 
			4'h5: LED[20:7] <= 7'b1101011; 
			4'h6: LED[20:7] <= 7'b1101111; 
			4'h7: LED[20:7] <= 7'b1010010; 
			4'h8: LED[20:7] <= 7'b1111111; 
			4'h9: LED[20:7] <= 7'b1111010; 
			4'hA: LED[20:7] <= 7'b1111110; 
			4'hB: LED[20:7] <= 7'b1111111; 
			4'hC: LED[20:7] <= 7'b1100101; 
			4'hD: LED[20:7] <= 7'b1110111; 
			4'hE: LED[20:7] <= 7'b1101101; 
			4'hF: LED[20:7] <= 7'b1101100; 
			default:LED[20:7] <= 7'bxxxxxxx; 
		endcase 
	 end 
	 
always @ (time_out[15:8]) 
	begin 
		 case(time_out[15:8]) 
			4'h0: LED[27:21] <= 7'b1110111; 
			4'h1: LED[27:21] <= 7'b0010010; 
			4'h2: LED[27:21] <= 7'b1011101; 
			4'h3: LED[27:21] <= 7'b1011011; 
			4'h4: LED[27:21] <= 7'b0111010; 
			4'h5: LED[27:21] <= 7'b1101011; 
			4'h6: LED[27:21] <= 7'b1101111; 
			4'h7: LED[27:21] <= 7'b1010010; 
			4'h8: LED[27:21] <= 7'b1111111;
			4'h9: LED[27:21] <= 7'b1111010;
			4'hA: LED[27:21] <= 7'b1111110; 
			4'hB: LED[27:21] <= 7'b1111111;
			4'hC: LED[27:21] <= 7'b1100101; 
			4'hD: LED[27:21] <= 7'b1110111;
			4'hE: LED[27:21] <= 7'b1101101;
			4'hF: LED[27:21] <= 7'b1101100; 
			default:LED[27:21] <= 7'bxxxxxxx; 
		endcase 
	end 
 
//------------------------------------------------------------------------------ 
reg I2Cclk =1; // I2C clock

reg [8:0] clkclk=clock_div;// in order to divide50MHz clock 
reg sda_int =1; // making SDA high in initial state 
reg [15:0] outreg =0; 
reg [3:0] I2C_counter=0; // for givingthe pointer address 
reg [3:0] bitaddrs7=4'b0001;// initializingvia givingslaveaddress(1001000-0(R/W)) 
reg [3:0] Rxcount=0; 
reg [3:0] slaveaddrs=0;//receivingdata from ds1307 
// ---------------------------------------------------------------------------- 
reg [2:0] state =Rxidle; // For State machine starting 
assign scl =chk?1'b1:I2Cclk;// Generated Clock for I2C 
assign time_out = outreg; // output from ds1307 
assign sda =sda_int; // UsingSDA pin 
assign ack_out=ack; 
assign ack_count1=ack_count; 
	
////------------------------------
always @ (posedge clk) 
/// --------------------- scl generation----------------- 
begin clkclk=clkclk - 1; 
	if(clkclk==0) 
		begin I2Cclk=~I2Cclk; 
		clkclk =clock_div; 
	end 
end

////............................................................................. 
always @(posedge I2Cclk )
	if(reset) begin chk <=1; 
		outreg <=0; 
		I2C_counter <=0; 
		bitaddrs7 <=4'b0001; 
		Rxcount <=0; slaveaddrs <=0; 
		ack <=0; ack_count <=0; 
		I2Cclk <=1; sda_int <=1; 
		clkclk <=clock_div; 
end
 
always @(posedge I2Cclk ) 
	begin 
	//....................................state machine............................ 
	case(state) 
		//------------------------------------------------------------------------------- 
		Rxidle: 
			begin sda_int=1'b0;
					//---------------------start sda = 0 
					chk=1'b0; //--------------------- startscl 
					state =Rxstart;
			end 
		//------------------------------------------------------------------------------- 

		Rxstart:
			begin 
				case(bitaddrs7) 
					// initial frame1101000 Initializingslaveaddress ds1307 
					1: 
						begin 
							sda_int =1'b1;	//---------------------sda 1 
							bitaddrs7=bitaddrs7+1;
						end 
					2: 
						begin 
							sda_int =1'b1;//---------------------sda 1 
							bitaddrs7=bitaddrs7+1;
						end 
					3: 
						begin 
							sda_int =1'b0;//---------------------sda 0 
							bitaddrs7=bitaddrs7+1;
						end 
					4: 
						begin sda_int =1'b1;//---------------------sda 1 
							bitaddrs7=bitaddrs7+1;
						end 
					5: 
						begin 
							sda_int =1'b0;//---------------------sda 0 
							bitaddrs7=bitaddrs7+1;
						end 
					6: 
						begin 
							sda_int =1'b0;//---------------------sda 0 
							bitaddrs7=bitaddrs7+1;
						end 
					7: 
						begin 
							sda_int =1'b0;//---------------------sda=0 
							bitaddrs7=bitaddrs7+1;
						end 
					8: 
						begin 
							sda_int =1'b0; 
							ack_count=2'b00; 
							bitaddrs7=bitaddrs7+1; // R/W bit 
						end 
					9: 
						begin 
							if(sda==1'b0) 
								ack[0]=1; 
								state=RxPointeraddr; // Checking ack 
						end 
				endcase 
			end 
			
			//--------------------------------------------------------------------------------- 
		RxPointeraddr: // Pointer Address 00000001 for minute register 
			begin 
				case(I2C_counter) //---------------------sda = 0 
					0: 
						begin 
							sda_int =1'b0; 
							I2C_counter=I2C_counter+1;
						end //---------------------sda = 0 
					1: 
						begin 
							sda_int =1'b0; 
							I2C_counter=I2C_counter+1;
						end //---------------------sda = 0 
					2: 
						begin 
							sda_int =1'b0; 
							I2C_counter=I2C_counter+1; 
						end //---------------------sda = 0 
					3: 
						begin 
							sda_int =1'b0; 
							I2C_counter=I2C_counter+1; 
						end //---------------------sda = 0 
					4: 
						begin 
							sda_int =1'b0; 
							I2C_counter=I2C_counter+1;
						end //---------------------sda = 0 
					5: 
						begin 
							sda_int =1'b0;
							I2C_counter=I2C_counter+1;
						end //---------------------sda = 0 
					6: 
						begin 
							sda_int =1'b0; 
							I2C_counter=I2C_counter+1;
						end //---------------------sda = 1 
					7: 
						begin 
							sda_int =1'b0; 
							ack_count=2'b01; 
							I2C_counter=I2C_counter+1;
						end 
					8: 
						begin 
							if(sda==1'b0) 
								ack[1]=1'b0; 
								state=Rxstart1;
						end // after pointer register sda line acknoledged by a low pulse 
				endcase 
			end 
		//-----------------------------------start+ slaveaddress byte--------------------- 
		Rxstart1: 
			begin 
				case(slaveaddrs) 
					// initial frame1101000 Initializingslaveaddress includingstart (case0:) 
					0: 
						begin 
							sda_int=1'b0; 
							slaveaddrs=slaveaddrs+1;
						end 
					1: 
						begin 
							sda_int =1'b1;//---------------------sda 1 
							slaveaddrs=slaveaddrs+1;
						end 
					2: 
						begin 
							sda_int =1'b1;//---------------------sda 1 
							slaveaddrs=slaveaddrs+1;
						end 
					3: 
						begin 
							sda_int =1'b0;//---------------------sda 0 
							slaveaddrs=slaveaddrs+1;
						end 
					4: 
						begin 	
							sda_int =1'b1;//---------------------sda 1 
							slaveaddrs=slaveaddrs+1;
						end 
					5: 
						begin 
							sda_int =1'b0;//---------------------sda 0 
							slaveaddrs=slaveaddrs+1;
						end 
					6: 
						begin 
							sda_int =1'b0;//---------------------sda 0 
							slaveaddrs=slaveaddrs+1;
						end 
					7: 
						begin 
							sda_int =1'b0;//---------------------sda=0 
							slaveaddrs=slaveaddrs+1;
						end 
					8: 
						begin 
							sda_int =1'b1; 
							slaveaddrs=slaveaddrs+1; // R/W bit high 
							ack_count=2'b10; 
						end 
					9: 
						begin 
							if(sda==1'b0) ack[2]=1; 
								state=Rxdata; 
								// Checking ack 	
						end 
				endcase 
			end 
				//----------------------------------receiving time -------------------------- 
		Rxdata: 
			begin 
				case(Rxcount)
					0: 
						begin 
							outreg[15]=sda; 
							Rxcount=Rxcount+1;
						end 
					1: 
						begin 
							outreg[14]=sda; 
							Rxcount=Rxcount+1;
						end 
					2: 
						begin 
							outreg[13]=sda; 
							Rxcount=Rxcount+1;
						end 
					3: 
						begin 
							outreg[12]=sda; 
							Rxcount=Rxcount+1;
						end 
					4: 
						begin 
							outreg[11]=sda; 
							Rxcount=Rxcount+1;
						end 
					5: 
						begin 
							outreg[10]=sda; 
							Rxcount=Rxcount+1;
						end 
					6: begin 
							outreg[9]=sda; 
							Rxcount=Rxcount+1;
						end 
					7: 
						begin 
							outreg[8]=sda; 
							Rxcount=Rxcount+1; 
							ack_count=2'b11; 
						end // low pulseacknoledgment from ds1307 
					8: 
						begin 
							outreg[7]=sda; 
							if(!sda) ack[2]=1; 
							Rxcount=Rxcount+1;
						end 
					9: 
						begin 
							outreg[6]=sda ; 
							Rxcount=Rxcount+1;
						end 
					10: 
						begin 
							outreg[5]=sda ; 
							Rxcount=Rxcount+1;
						end 
					11: 
						begin 
							outreg[4]=sda; 
							Rxcount=Rxcount+1;
						end 
					12: 
						begin 
							outreg[3]=sda; 
							Rxcount=Rxcount+1;
						end 
					13: 
						begin 
							outreg[2]=sda; 
							Rxcount=Rxcount+1;
						end 
					14: 
						begin 
							outreg[1]=sda; 
							Rxcount=Rxcount+1;
						end 
					15: 
						begin 
							sda_int=1'b0; // checking lowpulseacknoledge frm ds1307 
							state=Rxstop; 
						end 
				endcase 
			end 
		//----------------------------------------------------------------------------------------- 
		Rxstop: 
			begin 
				chk=1; 
				sda_int=1'b1;
				state= Rxidle; 
			end 
		endcase 
	end 
endmodule
 
