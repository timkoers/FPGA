module LCDmodule(clk, RxD, LCD_RS, LCD_RW, LCD_E, LCD_DataBus);
input clk, RxD;
ouput LCD_RS, LCD_RW, LCD_E;
output [7:0] LCD_DataBus;