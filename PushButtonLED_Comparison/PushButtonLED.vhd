library ieee;
use ieee.std_logic_1164.all;

entity PushButtonLED is
	Port (BUTTON1 : in std_logic;
			LED1 : out std_logic;
			LED2 : out std_logic;
			LED3 : out std_logic;
			LED4 : out std_logic;
			LED5 : out std_logic;
			LED6 : out std_logic;
			LED7 : out std_logic;
			LED8 : out std_logic;
			LED9 : out std_logic;
			LED10 : out std_logic;
			LED11 : out std_logic;
			LED12 : out std_logic;
			LED13 : out std_logic);
end PushButtonLED;

-- LED 1 = pin 3
-- LED 2 = pin 7

architecture Behavioral of PushButtonLED is
begin
	LED1 <= BUTTON1;
	LED2 <= BUTTON1;
	LED3 <= BUTTON1;
	LED4 <= BUTTON1;
	LED5 <= BUTTON1;
	LED6 <= BUTTON1;
	LED7 <= BUTTON1;
	LED8 <= BUTTON1;
	LED9 <= BUTTON1;
	LED10 <= BUTTON1;
	LED11 <= BUTTON1;
	LED12 <= BUTTON1;
	LED13 <= BUTTON1;
end Behavioral;